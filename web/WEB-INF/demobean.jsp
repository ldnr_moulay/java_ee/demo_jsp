<%-- 
    Document   : demobean
    Created on : 10 juin 2021, 11:10:09
    Author     : stag
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>demobean</title>
    </head>
    <body>
        <jsp:useBean id="p1" class="model.Produit" scope="page">
            <jsp:setProperty name="p1" property="nom" value="SHips"/>
            <jsp:setProperty name="p1" property="prix" value="25"/>
            <p>Un bean vient d'être crée</p>
            <p>
               Produit :<jsp:getProperty name="p1" property="nom" />
               Prix :<jsp:getProperty name="p1" property="nom" />
            </p>
        </jsp:useBean>
    </body>
</html>
