package model;

// Classe pour un Java Bean = Pojo (Plain Old Java Object)
// - constructeur sans paramètre
// - getters-setters
public class Produit {
    private String nom;
    private int prix;

    // Constructeurs
    public Produit(String nom, int prix) {
        this.nom = nom;
        this.prix = prix;
    }
    //Constructeur sans parametre obliger pour beans
    public Produit() {
    }

    // Getters-Setters doivent être public pour les beans
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
    
    
}
