<%-- 
    Document   : bonjour
    Created on : 8 mai 2017, 16:30:21
    Author     : vincent
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.time.LocalDateTime"
         import="java.time.format.DateTimeFormatter" %>
<%!
    String test = null;
    int rayon = 3, age = 19;

    public boolean estMajeur(int i) {
        return i >= 18;
    }
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" href="favicon.ico">
        <link rel="stylesheet" href="./css/style.css">
        <title>Bonjour</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/entete.jsp" %>
        <!-- Commentaire HTML envoyé au client -->
        <%-- Commentaire non envoyé au client --%>
        <h1>Bonjour à toutes et tous!</h1>
        <p>Aujourd'hui nous sommes le <%= LocalDateTime.now()
               .format(DateTimeFormatter.ofPattern("dd MMM yyyy"))%>.
        </p>


        <p>Périmètre = <%= 2 * Math.PI * rayon%></p>
        
        <p>Il est <%= estMajeur(age) ? "majeur" : "mineur"%>.</p>
        
<% for (int i=0; i<10; i++) { %>
<p>paragraphe n° <%= i %></p>
<% } %>

    </body>
</html>
