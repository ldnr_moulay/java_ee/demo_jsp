<%-- 
    Document   : newjsp
    Created on : 7 juin 2021, 10:51:26
    Author     : vincent
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@ include file="/WEB-INF/entete.jsp" %>

        <h1>Hello World!</h1>
        <jsp:useBean id="soda" class="model.Produit" scope="request">
            <p>
                Un bean a été crée
            </p>
        </jsp:useBean>
            <div>
                <p>Produit : <jsp:getProperty name="soda" property="nom"/><br>
                   Prix : <jsp:getProperty name="soda" property="prix"/><br>
                   Prix ttc : ${soda.prix*1.2}<br>
                   Nom affiché plus facilement : ${soda.nom}
                </p>
            </div>
    </body>
</html>
